from unittest import TestCase
from hermes import MPLogger


class TestContextManager(TestCase):
    """Test the logger used as a context manager and decorator"""

    def test_context_manager_exit(self):
        """The logger's AsyncResult is ready when the context ends"""

        external_reference = None
        with MPLogger() as logger:
            initial_state = logger.result.ready()
            self.assertIs(initial_state, False)
            external_reference = logger
        self.assertIs(external_reference.result.ready(), True)
