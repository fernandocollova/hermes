# Copyright (c) 2017 Fernando Collova
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from setuptools import setup

dist = setup(
    name='hermes',
    packages=['hermes'],
    version="8.2.4",
    description='Hermes async logger',
    install_requires=[
        "pushbullet.py",
        "python-dateutil",
        "psycopg2>=2.7,<2.8"
    ],
)

from hermes import DefaultLogfile

logfile = DefaultLogfile(dist.metadata.name)
logfile.create_and_chown()

# Install with:
# python3 -m pip install --upgrade (--no-cache-dir)
# git+ssh://git@bitbucket.org/fernandocollova/hermes.git#egg=hermes
