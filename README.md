# Hermes asyncronic logger

This project is a multi-process based implementation of the python standar logging module, adding to it 2 advantages:

- The first is speed, as the I/O is managed in a sepparate process, the main thread doesn't have to wait for the disk or network to finish it's part.

- And the other is isolation, which is accomplished by running each logger in it's own fresly started python process. This means that Django will never overwrite your desired logging level again.

Also, there are 2 extra handlers that proved themselves useful:

- Pushbullet, for push notifications

- DB, for logging to PostgreSQL. This requires Psycopg2>2.7, which is a C module, so you might find problems if pip doesn't have a pre-built binary. If taht's the case, you'll have to search [here](http://initd.org/psycopg/docs/install.html) for the best way to install it in your system.
