# Copyright (c) 2017 Fernando Collova
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

class MissingAuth(Exception):
    pass

class FinishLoggerProcess(Exception):
    """
    Exception to raise to finish the logger process cleanly
    """
