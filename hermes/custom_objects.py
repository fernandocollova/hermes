# Copyright (c) 2017 Fernando Collova
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from hermes.exceptions import FinishLoggerProcess
import logging

class CustomLogger(logging.Logger):
    def finish_logger_process(self, message, extra):
        self.debug(message, extra=extra)
        raise FinishLoggerProcess()

def raise_everything(exception):
    """Callback for errors in pool functions"""
    raise exception
