# Copyright (c) 2017 Fernando Collova
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from sys import stdout

config = {

    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "verbose": {
            "format": "%(asctime)16s - %(source)s - %(levelname)s - %(message)s",
            "datefmt": "%Y-%m-%d %H:%M"
        },
        "push": {
            "format": "%(asctime)16s - %(message)s",
            "datefmt": "%Y-%m-%d %H:%M"
        }
    },
    "handlers": {
        "console": {
            "level": "INFO",
            "class": "logging.StreamHandler",
            "stream": stdout,
            "formatter": "verbose"
        },
        "file": {
            "level": "DEBUG",
            "class": "logging.FileHandler",
            "filename": "./hermes.log",
            "formatter": "verbose"
        },
        "push": {
            "level": "CRITICAL",
            "class": "hermes.push_handler.PushHandler",
            "auth": "YOUR_PUSHBULLET_AUTH_HERE",
            "formatter": "push"
        },
        "db": {
            "level": "INFO",
            "class": "hermes.db_handler.DbHandler",
            "engine": "postgres",
            "host": "localhost",
            "port": 8765, # 5432 is PG default, but I use this for testing
            "user": "hermes",
            "pwd": "hermes",
            "db":"hermes",
            "table": "hermes",
        }
    },
    "loggers": {
        "hermes": {
            "handlers": ["console", "file", "push", "db"],
            "level": "DEBUG",
            "propagate": True
        }
    }
}
