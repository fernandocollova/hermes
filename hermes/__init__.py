# Copyright (c) 2017 Fernando Collova
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from sys import exc_info
from os import getuid, chown, getenv, path
import multiprocessing as mp
import traceback
import inspect
from hermes.broker import logger_process, broker
from hermes.custom_objects import raise_everything


class MPLoggerRegister(type):

    def __call__(cls, name=None, config=None):
        """
        Register for all the created loggers. This code takes care of saving
        all the MPLogger objects for closing them with the close_all_loggers
        method, and to return them when a logger with an existing name is asked
        to be created. Basically, it turns each logger name into a singleton.
        """

        if not config:
            config = cls.default_config

        stack = [name[3] for name in inspect.stack()][1:]
        stack.reverse()
        if not name:
            name = ".".join(stack)

        if cls.loggers.get(name):
            return cls.loggers.get(name)
        cls.loggers[name] = super(MPLoggerRegister, cls).__call__(name, config)
        cls.update_broker()
        return cls.loggers[name]


class MPLogger(metaclass=MPLoggerRegister):
    """
    Class-level code to coordinate the MPLogger instances. The methods here can
    assign class-level attributes to the instances, destroy all the loggers,
    and coordinate the broker creation and update. In other words, anything one
    level abouve the object's responsabilities, goes here.

    For that coordination, two set of pipes are used:

    - messages_pipes: For sending messages to the broker, or signaling it has
    to end
    - current_unlock_receiver (or control_pipe on the broker): To signal the
    broker it can start handling messages.

    Those pipes, are used with (roughly) the following logic:

    - When the frist logger starts, it has an already waiting unlock token on
    the control_pipe, so it can start right away reading messages from the
    messages_pipe.

    - When a new logger is requested, and needs to be created, the creation
    process start with the call to __new__ which creates the object and the
    logger process attached to it.

    - Before returning that object, the logger is added to the 'loggers' dic
    and a new broker is created with the updated list of loggers.

    - The creation process begins with sending a None message trough the
    message pipe. That will kill the old broker when it's read, but before
    exiting, the old broker will send an unlock message trough the
    control_pipe, so the new broker knows it can start.

    - While that happens, a new broker is created, which will be waiting for
    the unlock token in it's control_pipe before reading messages from the
    messages pipe.

    - When that finishes, the new logger object is returned

    This logic ensures not only that just one logger will be reading from the
    messages pipe, but that the "old" logger won't receive a message for a
    logger not in it's logger list, because the new object's messages will all
    be queed before the None destruction token.

    Also, concurrency between the broker processes is guaranteed, as the way
    they start is "chained" to each other, more or less like a chained list.
    See the connect_pipes method for more details on that.
    """

    loggers = {}
    pool = None
    broker_result = None
    current_unlock_receiver = None
    default_config = "hermes.default_config"
    mp_context = mp.get_context("fork")
    messages_pipes = mp_context.Pipe(duplex=False)

    @classmethod
    def _get_pipes(cls):
        return {
            cls.loggers[key].name:
            cls.loggers[key].pipes for key in cls.loggers
        }

    @classmethod
    def get_or_create_pool(cls):

        if cls.pool is None:
            cls.pool = cls.mp_context.Pool()
        return cls.pool

    @classmethod
    def connect_pipes(cls):
        """
        Chain the signals from the current and next broker.
        The objective of this method is to create a custom pipe, in which the
        sending end is brand-new, but the receiving end gets the unlock signal
        from the current broker. That means, that the receiving end is the
        receiving end from the soon to be defunct old broker, which will send
        an unlock token to the new one when it cleanly exits.
        """

        new_unlock_receiver, new_unlock_sender = cls.mp_context.Pipe(
            duplex=False)
        new_pipes = cls.current_unlock_receiver, new_unlock_sender
        cls.current_unlock_receiver = new_unlock_receiver
        return new_pipes

    @classmethod
    def update_broker(cls):
        """
        Code for creating or "updating the list of loggers" the broker has.
        Updating the list of loggers goes between quotes, because no list is
        ever updated, instead when there is a new item in the list, a new
        broker is created with the new list. That broker, altrough it starts to
        run in the moment there is a new logger, has a logic to coorfinate when
        it actually starts to operate:

        - When the first logger is created, the first thing it does is wait to
        receive a token trough a pipe before doing the actual broker job.
        That token is sent trough the 'current_unlock_receiver' or control pipe
        on the broker code, which is created as a result of the first if.

        - When the next loggers are created, each one triggers the destruction
        of the prvious broker, and the creation of a new one. That new broker,
        as the previous one, it's locked, and the other end of the pipe used to
        unlock it is on the deprecated logger about to finish.
        """

        if cls.broker_result:
            cls.messages_pipes[1].send(None)
            pipes = cls.connect_pipes()
        else:
            pipes = cls.mp_context.Pipe(duplex=False)
            pipes[1].send(True)
            cls.current_unlock_receiver = pipes[0]

        cls.broker_result = cls.pool.apply_async(
            func=broker,
            args=(cls._get_pipes(), pipes, cls.messages_pipes[0]),
            error_callback=raise_everything
        )

    @classmethod
    def _unregister_logger(cls, name):
        del cls.loggers[name]

    @classmethod
    def close_all_loggers(cls):
        """
        Closees all loggers, broker process and pool. The loggers_list var
        exists because at close time, each logger removes itself from the
        class level loggers list, and the list cannot change size during  the
        iteration. The process to close the broker process and pool is set so
        that if after closing, you request a new logger, it can start over
        normally, as the class expects some thing to be in a specific state
        """

        loggers_list = cls.loggers.copy()
        for obj in loggers_list.values():
            obj.wait_until_done()

        if cls.broker_result:
            cls.messages_pipes[1].send(None)  # Kill the broker process
            cls.current_unlock_receiver.recv()  # Wait for the unlock token
            cls.broker_result.get()  # Wait for the process to finish
            cls.broker_result = None  # Clear it so it can be created agin
            cls.pool.close()  # Stop the pool
            cls.pool.join()  # Wait for it to finish
            cls.pool = None  # Clear it so it can be created agin

    def __new__(cls, name, config):
        """
        The code here saves class-level attributes to the newly created
        instance.
        """
        new_instance = super(MPLogger, cls).__new__(cls)
        new_instance.pipes = cls.mp_context.Pipe()
        new_instance.messages_pipe = cls.messages_pipes[1]
        new_instance.pool = cls.get_or_create_pool()
        return new_instance

    def __init__(self, name=None, config=None):
        """
        The actuaal code of the instance goes from here till the end of the
        class.

        The init's one and only job is to start the logger process once all
        the preparation was done.

        And the rest of the methods are mostly conveninece methods for sending
        messages trough the messages_pipe.

        That simplicity it's actually by design, as the objective of the object
        is to get rid of the messages as fast as possible, to avoid locking the
        main thread as much as possible.
        """

        self.name = name
        self.result = self.pool.apply_async(
            func=logger_process,
            args=(self.pipes, config, name),
            error_callback=raise_everything
        )

    def _send(self, level, message):

        try:
            source = inspect.stack()[2][3]
        except IndexError:  # Assume is called inside a context
            source = self.name

        self.messages_pipe.send(
            {
                "logger_name": self.name,
                "level": level,
                "message": message,
                "source": source
            }
        )

    def critical(self, message):
        self._send("critical", message)

    def fatal(self, message):
        self._send("fatal", message)

    def error(self, message):
        self._send("error", message)

    def warning(self, message):
        self._send("warning", message)

    def info(self, message):
        self._send("info", message)

    def debug(self, message):
        self._send("debug", message)

    def send_error(self):
        trace = ''
        for item in traceback.format_exception(*exc_info()):
            trace += item
        self._send("critical", trace)

    def __del__(self):
        """Ensure the logger is properly closed.

        That means, if the process hasn't sucessfully finshed yet, send the
        close message and wait for the process to finish.
        """

        try:
            self.result.successful()
        except AttributeError as attr_exception:
            del attr_exception
        except AssertionError as assert_exception:
            del assert_exception
            self._send(
                level="finish_logger_process",
                message="Last message from {}. Goodbye.".format(self.name)
            )
            self.result.get()

    def wait_until_done(self):
        self.__del__()

    def __enter__(self):
        return self

    def __exit__(self, exception_type, exception_value, trace):
        self.__del__()


close_all_loggers = MPLogger.close_all_loggers


class DefaultLogfile(object):
    """
    This class objective is to provide sane defaults and convenience methods
    for a default logfile.

    Given a package name, it can create a logfile with proper user ownership,
    taking info from env variables.

    Also, provided with the same environment, it can retrive info about the
    created default file.

    The env vars that influence its behaibor are the following:

    'package_name'_LOGS_PATH:
    This env will be auto generated from the package name and if found, will
    be used when creating the logfile or returning it's default path

    'package_name'_UID and 'package_name'_GID will be used to chown the logifle
    in case it's created.

    Without those varaibles, the logic used for creating and retiving the file
    or file path is one of this two:

    If using get_or_create:

    - The init will try to create/open the file from
      /var/log/'package_name'.log
    - If it cannot be opened for writing, it will create/open it
      from ~/.'package_name'.log
    - If that fails, it will raise an error

    This method is useful to use as a sane default, as it will place a logfile
    in an "appropriate" location for the user running the script.

    If using create_and_chown:

    This will do all of the above, and if it doesn't fail, it will try to
    chown the logfile with the 'package_name'_UID and 'package_name'_GID envs,
    or the current user's UID and GID.

    This method can be useful to use on a setup.py that's ran by root, to
    create a logfile under /var that's own by the user running the process that
    will generate the logs.
    """

    def __init__(self, name):
        self.logger_name = name + "_logfile"
        self.package_name = name
        self.path_env = name.upper() + "_LOGS_PATH"
        self.default_root_path = "/var/log/" + name + ".log"
        self.default_user_path = path.expanduser("~/.") + name + ".log"

    def get_or_create(self):

        try:
            logfile_path = getenv(self.path_env, self.default_root_path)
            logfile = open(logfile_path, "at")
            logfile.close()
        except PermissionError:
            if getenv(self.path_env):
                raise
            else:
                logfile_path = self.default_user_path

        return logfile_path

    def create_and_chown(self, uid=None, gid=None):

        chown(
            self.get_or_create(),
            uid if uid else getenv(self.package_name + "_UID", getuid()),
            gid if gid else getenv(self.package_name + "_GID", getuid())
        )
