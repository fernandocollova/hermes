# Copyright (c) 2017 Fernando Collova
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from sys import stdout
from hermes import DefaultLogfile

logfile = DefaultLogfile(__name__.split(".")[0])

config = {

    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "verbose": {
            "format": "%(asctime)16s - %(source)s - %(levelname)s - %(message)s",
            "datefmt": "%Y-%m-%d %H:%M"
        }
    },
    "handlers": {
        "console": {
            "level": "INFO",
            "class": "logging.StreamHandler",
            "stream": stdout,
            "formatter": "verbose"
        },
        logfile.logger_name: {
            "level": "DEBUG",
            "class": "logging.handlers.RotatingFileHandler",
            "maxBytes": 10 * 1024 * 1024,  # Clearer than 10MB in bytes
            "backupCount": 1,
            "filename": logfile.get_or_create(),
            "formatter": "verbose"
        }
    },
    "loggers": {
        "hermes": {
            "handlers": ["console", logfile.logger_name],
            "level": "DEBUG",
            "propagate": True
        }
    }
}
