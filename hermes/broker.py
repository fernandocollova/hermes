# Copyright (c) 2017 Fernando Collova
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


def broker(loggers_pipes, control_pipe, messages_pipe):
    """
    The objective of this function is to read messages from the messages_pipe,
    and pipe them to the corresponding process for logging. To avoid
    concurrency issues, after piping the message, it waits fo a messsage to be
    piped back before proceding to the next item in the messages_pipe
    """
    control_pipe[0].recv()
    while True:
        message = messages_pipe.recv()
        try:
            pipe = loggers_pipes[message["logger_name"]]
        except TypeError as e:
            if message is None:
                control_pipe[1].send(True)
                del e
                break
            else:
                raise e
        else:
            pipe[1].send(message)
            pipe[1].recv()


def logger_process(pipe, config, name):
    """
    This function sets up a logger with the configs in a provided python-path,
    and sends messages to it as they arrive from a pipe. To avoid concurrency
    issues, it sends back a 'True' from the pipe when the log has been processed
    """

    import logging
    import logging.config
    from importlib import import_module
    import hermes.custom_objects
    from hermes.exceptions import FinishLoggerProcess

    logging.setLoggerClass(hermes.custom_objects.CustomLogger)
    logging.config.dictConfig(import_module(config).config)
    logger = logging.getLogger(name)

    try:
        while True:
            item = pipe[0].recv()
            method = getattr(logger, item["level"])
            method(
                item["message"],
                extra={
                    "source": item["source"]
                }
            )
            pipe[0].send(True)
    except FinishLoggerProcess as exception:
        del exception
    finally:
        logging.shutdown()
        pipe[0].send(True)
