# Copyright (c) 2017 Fernando Collova
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import logging
from psycopg2 import sql, connect
from datetime import datetime
from dateutil.tz import tzlocal

class DbHandler(logging.Handler):

    def __init__(self, engine, host, port, user, pwd, db, table, level=0):

        if engine == "postgres":
            self.connection = connect(
                user=user,
                password=pwd,
                database=db,
                host=host,
                port=port
            )
            self.cursor = self.connection.cursor()

            self.statement = sql.SQL(
                "INSERT INTO {} (message, source, level, asctime) VALUES "
                "(%s, %s, %s, %s);").format(sql.Identifier(table))
        else:
            raise NotImplementedError("Only postgres is supported for now")

        super(DbHandler, self).__init__(level)

    def emit(self, record):

        self.cursor.execute(
            self.statement,
            (
                record.message,
                record.source[:100],
                record.levelno,
                datetime.fromtimestamp(record.created).replace(tzinfo=tzlocal())
            )
        )
        self.connection.commit()

    def close(self):
        self.cursor.close()
        self.connection.close()
        super(DbHandler, self).close()
