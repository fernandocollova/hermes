# Copyright (c) 2017 Fernando Collova
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import logging
from pushbullet import Pushbullet
from hermes.exceptions import MissingAuth

class PushHandler(logging.Handler):

    def __init__(self, auth, level=0):
        if not auth:
            raise MissingAuth(
                'ENV variable "PBULLET_AUTH" is not set ' + \
                'and "pbullet_auth" argument is missing'
                )
        self.sender = Pushbullet(auth)
        super(PushHandler, self).__init__(level)

    def emit(self, record):
        self.sender.push_note(
            "New {} message from {}".format(record.levelname, record.source),
            self.format(record)
        )
